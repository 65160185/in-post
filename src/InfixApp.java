import java.io.IOException;

public class InfixApp {
    public static void main(String[] args) throws IOException {
        String input, output;
        while(true) {
            System.out.print("Enter infix: ");
            System.out.flush();
            input = getString();
            if( input.equals("") ) {
                break;
            }
            InToPost theTrans = new InToPost(input);
            output = theTrans.doTrans();
            System.out.println("Postfix is " + output + '\n');
        }
    }


    public static String getString() throws IOException {
        byte[] buffer = new byte[1024];
        int len = System.in.read(buffer);
        return new String(buffer, 0, len);
    }
}
